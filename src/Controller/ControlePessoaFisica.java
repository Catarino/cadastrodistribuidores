/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.util.ArrayList;
import model.PessoaFisica;

/**
 *
 * @author THAIS
 */
public class ControlePessoaFisica {
    private ArrayList<PessoaFisica> listaPessoaFisica;
    public ControlePessoaFisica(){
        listaPessoaFisica = new ArrayList<PessoaFisica>();
    }
    public String adicionaPessoaFisica(PessoaFisica umaPessoa){
        listaPessoaFisica.add(umaPessoa);
        return "Pessoa Fisica adicionada com sucesso!";
    }
    public String removerPessoaFisica(PessoaFisica umaPessoa){
        listaPessoaFisica.remove(umaPessoa);
        return "Pessoa Fisica removida com sucesso!";
    }
    public PessoaFisica buscarPessoaFisica(String umNome){
        for(PessoaFisica umaPessoa : listaPessoaFisica){
            if(umaPessoa.getNome().equalsIgnoreCase(umNome))
                return umaPessoa;
        }
        return null;
    }
    public void mostrarPessoaFisica(){
        System.out.println("=================================");
        System.out.println("    Lista de Pessoas Fisicas: ");
        for(PessoaFisica umaPessoa : listaPessoaFisica){
            System.out.println("    "+umaPessoa.getNome());
            System.out.println("    "+umaPessoa.getCpf());
            System.out.println("    "+umaPessoa.getTelefone());
        }
        System.out.println("=================================");
    }
}
