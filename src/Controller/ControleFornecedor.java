/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

/**
 *
 * @author THAIS
 */
public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedores;

    public ArrayList<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(ArrayList<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }
    public ControleFornecedor(){
        listaFornecedores = new ArrayList<Fornecedor>();
    }
    //Pessoa Fisica
    public String adicionarFornecedor(PessoaFisica umaPessoa){
        listaFornecedores.add(umaPessoa);
        return "Pessoa Fisica adicionada com sucesso!";
    }
    //Pessoa Juridica
    public void adicionarFornecedor(PessoaJuridica umaPessoa){
        listaFornecedores.add(umaPessoa);
    }
    public void adicionarFornecedor(Fornecedor umFornecedor){
        listaFornecedores.add(umFornecedor);
    }
    public String removerFornecedor(PessoaFisica umaPessoa){
        listaFornecedores.remove(umaPessoa);
        return "Pessoa Fisica removida com sucesso!";
    }

    public void removerFornecedor(PessoaJuridica umaPessoa){
        listaFornecedores.remove(umaPessoa);
    }
    public void removerFornecedor(Fornecedor umFornecedor){
        listaFornecedores.remove(umFornecedor);
    }
    
    public void mostrarPessoaJuridica(PessoaJuridica umaPessoa){
        listaFornecedores.add(umaPessoa);
    }
    public Fornecedor buscarFornecedor(String umNome){
        for(Fornecedor umFornecedor : listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umNome))
                return umFornecedor;
        }
        return null;
    }
    public void mostrarFornecedores(){
        System.out.println("=================================");
        System.out.println("    Lista de Pessoas Fisicas: ");
        for(Fornecedor umFornecedor : listaFornecedores){
             System.out.println("    "+umFornecedor.getNome());
            System.out.println("    "+umFornecedor.getTelefone());
        }
        System.out.println("=================================");
    }
}
