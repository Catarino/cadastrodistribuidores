/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.util.ArrayList;
import model.Produto;

/**
 *
 * @author THAIS
 */
public class ControleProduto {
    private ArrayList<Produto> listaProdutos;

    public ArrayList<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(ArrayList<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }
    
    public ControleProduto(){
        listaProdutos = new ArrayList<Produto>();
    }
    public String adicionarProduto(Produto umProduto){
        listaProdutos.add(umProduto);
        return "Produto adicionado com sucesso!";
    }
    public String removeProduto(Produto umProduto){
        listaProdutos.remove(umProduto);
        return "Produto removido com sucesso!";
    }
    public Produto buscaProduto(String nomeProduto){
        for(Produto umProduto : listaProdutos){
            if(umProduto.getNome().equalsIgnoreCase(nomeProduto))
                return umProduto;
            
        }
        return null;
    }
    public void mostrarProduto(){
        for(Produto umProduto : listaProdutos){
            System.out.println("Produto: "+umProduto.getNome());
        }
    }
}
