/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.util.ArrayList;
import model.PessoaJuridica;

/**
 *
 * @author THAIS
 */
public class ControlePessoaJuridica {
    private ArrayList<PessoaJuridica> listaPessoaJuridica;
    public ControlePessoaJuridica(){
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
    }
    public String adicionarPessoaJuridica(PessoaJuridica umaPessoa){
        listaPessoaJuridica.add(umaPessoa);
        return "Pessoa Juridica adicionada com sucesso!";
    }
    public String removerPessoaJuridica(PessoaJuridica umaPessoa){
        listaPessoaJuridica.remove(umaPessoa);
        return "Pessoa Juridica removida com sucesso!";
    }
    public PessoaJuridica buscarPessoaJuridica(String umNome){
        for(PessoaJuridica umaPessoa : listaPessoaJuridica){
            if(umaPessoa.getNome().equalsIgnoreCase(umNome))
                return umaPessoa;
        }
        return null;
    }
    public void mostrarPessoaJuridica(){
        System.out.println("=================================");
        System.out.println("    Lista de Pessoas Fisicas: ");
        for(PessoaJuridica umaPessoa : listaPessoaJuridica){
             System.out.println("    "+umaPessoa.getNome());
            System.out.println("    "+umaPessoa.getCnpj());
            System.out.println("    "+umaPessoa.getTelefone());
        }
        System.out.println("=================================");
    }
}
