/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author THAIS
 */
public class Produto {
    private String nome;
    private String Descricao;
    private double valorCompar;
    private double valorVenda;
    private double quantidadeEstoque;
    
    public Produto(){
        
    }
    public Produto(String nome, String Descricao, double valorCompra, double valorVenda, double quantidadeEstoque){
        this.nome = nome;
        this.Descricao = Descricao;
        this.valorCompar = valorCompra;
        this.valorVenda = valorVenda;
        this.quantidadeEstoque = quantidadeEstoque;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public double getValorCompar() {
        return valorCompar;
    }

    public void setValorCompar(double valorCompar) {
        this.valorCompar = valorCompar;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }

    public double getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(double quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    
}
