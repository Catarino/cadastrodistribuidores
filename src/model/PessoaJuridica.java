/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author THAIS
 */
public class PessoaJuridica extends Fornecedor{
    private String cnpj;
    private String razaoSocial;
    
    public PessoaJuridica(String nome, String telefone){
        super(nome, telefone);
    }
    public PessoaJuridica(String nome, String telefone, String cnpj){
        super(nome, telefone);
        this.cnpj = cnpj;
    }
    public PessoaJuridica(String nome, String telefone, Produto produtos, Endereco endereco, String cnpj, String razaoSocial){
        super(nome, telefone, produtos, endereco);
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
}
