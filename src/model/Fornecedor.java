/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;

/**
 *
 * @author THAIS
 */
public class Fornecedor {
    private String nome;
    private ArrayList<String> telefone = new ArrayList<String>();
    private ArrayList<Produto> produtos = new ArrayList<Produto>();
    private Endereco endereco;
    public Fornecedor(){
        
    }
    public Fornecedor(String nome, String telefone, Produto produtos, Endereco endereco){
        this.nome = nome;
        this.telefone.add(telefone);
        this.produtos.add(produtos);
        this.endereco = endereco;
        
    }
    public Fornecedor (String nome, String telefone) {
	this.nome = nome;
        this.telefone.add(telefone);
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getTelefone() {
        return telefone;
    }

    public void setTelefone(ArrayList<String> telefone) {
        this.telefone = telefone;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
