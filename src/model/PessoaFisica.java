/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author THAIS
 */
public class PessoaFisica extends Fornecedor{
    private String cpf;

    public PessoaFisica(String nome, String telefone){
        super(nome,telefone);
    }
    public PessoaFisica(String nome, String telefone, String cpf){
        super(nome,telefone);
        this.cpf = cpf;
    }
    public PessoaFisica(String nome, String telefone, Produto produtos, Endereco endereco, String cpf){
        super(nome, telefone, produtos, endereco);
        this.cpf = cpf;
    }
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
